let MVDBKEY = 'bed57bace403c25c78cbb5feeae4afd5';

//state
const state = ()=>({
  moviesList : [],
  singleMovie : [],
  tvGenere : [],
  latestTv : [],
  tvGenereType : [],
  movieSearchByName : [],
})


//getters
const getters ={}


//actions
const actions = {

  //get list of movies
  async getMoviesList({commit}) {
    //const API_KEY = 'bed57bace403c25c78cbb5feeae4afd5';
    const API_URL = `https://api.themoviedb.org/3/movie/popular?api_key=${MVDBKEY}&language=en-US&page=1`;

    await fetch(API_URL)
      .then((response) => {
        return response.json();
      })

      .then((jsonData) => {
        commit('addMovies', jsonData.results);
        return jsonData.results
      });
  },

  //get movie by id
  async getMovieById({commit}) {
    let url = ` https://api.themoviedb.org/3/movie/${this.$route.params.id}/videos?api_key=${MVDBKEY}&language=en-US`
    await fetch(url)
      .then((response) => {
        return response.json();
      }).then(data => {
        commit('addSingleMovieId', jsonData.results);
        return jsonData.results
      })
  },

  //get list of tv category
  async getTvGenere({commit}) {
    await fetch(`https://api.themoviedb.org/3/genre/tv/list?api_key=${MVDBKEY}&language=en-US`).then(res => {
      return res.json();
    }).then(data => {
      //console.log(data)
      commit('addTvGenere', data.genres);
    })
  },

  //get tv latest
  async getLatestTv({commit}){
    await fetch(`https://api.themoviedb.org/3/tv/latest?api_key=${MVDBKEY}&language=en-US`).then(res => {
      return res.json();
    }).then(data => {
      commit('addLatestTv', data);
    })
  },


  //get list tv genere
  async getGenreTvType({commit},genere){
    await fetch(`https://api.themoviedb.org/3/search/collection?api_key=${MVDBKEY}&language=en-US&query=${genere}&page=1`).then((response) => {
      return response.json();
    }).then(data => {
      commit('addListGenereTv', data.results)
    })
  },

  //search movie by name
  async searchMovieByName({commit},text) {
    let url = `https://api.themoviedb.org/3/search/movie?api_key=${MVDBKEY}&language=en-US&query=${text}&page=8&include_adult=false`
    await fetch(url)
      .then((response) => {
        return response.json();
      }).then(data => {
        commit('addMovieSearch', data.results)
      })
  },
}

//mutation
const mutations = {
  //add movies to state
  addMovies(state,movies){
    state.moviesList.push(...movies)
  },

  //add single movie by id
  addSingleMovieId(state,singleMovie){
    state.singleMovie.push(...singleMovie)
  },
//add tv genere list
  addTvGenere(state,tv){
    state.tvGenere = [];
    state.tvGenere.push(...tv)
  },

//add movie to state from search input
  addMovieSearch(state,search){
    state.movieSearchByName = [];
    state.movieSearchByName.push(...search);
  },

  //add list of latest tv series
  addLatestTv(state,list){
    state.latestTv.push(list)
  },

  // add list of tv series genere
  addListGenereTv(state,genre){
    state.tvGenereType.push(...genre)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
